package com.hackaton.ofuscate.service.impl;

import com.hackaton.ofuscate.service.OfuscateService;
import com.hackaton.ofuscate.utils.OfuscacionInformacionSensibleUtil;
import org.springframework.stereotype.Service;

@Service
public class OfuscateServiceImpl implements OfuscateService {

    @Override
    public String ofuscteText(String msg) {
        return OfuscacionInformacionSensibleUtil.ofuscarInformacionSensible(msg);
    }

}
