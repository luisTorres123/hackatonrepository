package com.hackaton.ofuscate;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OfuscateApplication {

	public static void main(String[] args) {
		SpringApplication.run(OfuscateApplication.class, args);
	}

}
