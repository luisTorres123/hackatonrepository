package com.hackaton.ofuscate.controller;

import com.hackaton.ofuscate.models.Message;
import com.hackaton.ofuscate.service.OfuscateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequestMapping(Routes.BASE + "/ofuscate")
public class OfuscateController {

    @Autowired
    OfuscateService ofuscateService;

    @PostMapping
    public Message ofuscteText(@RequestBody Message message) {
        try {
            final Message response = new Message();
            String msg = this.ofuscateService.ofuscteText(message.description);
            response.description = msg;
            return response;
        } catch (Exception e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

}
